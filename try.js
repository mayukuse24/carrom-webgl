var scene, camera, renderer;
var geometry, material, base,dummy;
var edgeu,edged,edgel,edger;
var striker,powermeter,path;
var piece = [0,0,0,0,0,0,0,0,0];
var corners = [0,0,0,0];
var friction = -0.01,eloss=0.8,netscore=100;
var scorevar,toggle=0;
var then,now;

init();
animate();

function init()
{
    scene = new THREE.Scene();
    timer = new THREE.Clock();
    console.log(timer);
    timer.running = true;
    camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 1000 );
    camera.position.x = 0;
    camera.position.y = 0;
    camera.position.z = 400;

    var spotLight1 = new THREE.SpotLight(0xffffff);
    spotLight1.position.set(30, 30, 100);
    spotLight1.castShadow = true;
    scene.add(spotLight1);

    var a = new THREE.Vector3( 0, 0, 0 );
    camera.lookAt(a);

    baseTexture = new THREE.TextureLoader().load("textures/cb.jpg");
    geometry = new THREE.BoxGeometry( 240,240,20);
    material = new THREE.MeshBasicMaterial( { map: baseTexture} );

    base = new THREE.Mesh( geometry, material );
    scene.add( base );

    geometry = new THREE.BoxGeometry(15,15,20);
    material = new THREE.MeshBasicMaterial( { color:0x000000} );
    for(var i=0;i<4;i++)
    {
	corners[i] = new THREE.Mesh( geometry, material );
	scene.add(corners[i]);
    }

    corners[0].position.x = -100;
    corners[0].position.y = 100;

    corners[1].position.x = -100;
    corners[1].position.y = -100;

    corners[2].position.x = 100;
    corners[2].position.y = 100;

    corners[3].position.x = 100;
    corners[3].position.y = -100;
    
    //geometry = new THREE.BoxGeometry( base.geometry.parameters.width,base.geometry.parameters.height,base.geometry.parameters.depth);
    geometry = new THREE.BoxGeometry(20,260,30);
    edgeTexture = new THREE.TextureLoader().load("textures/woodtexture.jpg");
    material = new THREE.MeshBasicMaterial( { map: edgeTexture} );
    
    edgel = new THREE.Mesh( geometry, material );
    scene.add( edgel );

    edger = new THREE.Mesh( geometry, material );
    scene.add( edger );

    geometry = new THREE.BoxGeometry(260,20,30);

    edgeu = new THREE.Mesh( geometry, material );
    scene.add( edgeu );

    edged = new THREE.Mesh( geometry, material );
    scene.add( edged );

    edgel.position.x = -base.geometry.parameters.width/2;
    edger.position.x = base.geometry.parameters.width/2 ;
    edged.position.y = -base.geometry.parameters.height/2;
    edgeu.position.y = base.geometry.parameters.height/2 ;
    
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );

    geometry = new THREE.CylinderGeometry( 5, 5, 2, 32 );
    material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
    striker = new THREE.Mesh( geometry, material );
    scene.add( striker );
    striker.position.x = 0;
    striker.position.y = -70;
    striker.position.z = 12;
    striker.speedx = 0;
    striker.speedy = 0;
    striker.rotateX(-90*Math.PI/180.0);
    striker.mass = 10;
    striker.theta = 0;
    striker.state = "inactive";
    
    geometry = new THREE.CylinderGeometry( 4, 4, 2, 32 );
    material = new THREE.MeshBasicMaterial( {color: 0xff0000} );
    piece[8] = new THREE.Mesh( geometry, material );
    piece[8].position.x = piece[8].position.y = 0;
    piece[8].position.z = 12;
    piece[8].mass = 4;
    piece[8].speedx=piece[8].speedy=0;
    piece[8].flag = -1;
    piece[8].type = "queen";
    piece[8].rotateX(-90*Math.PI/180.0);
    scene.add(piece[8]);
    
    geometry = new THREE.CylinderGeometry( 4, 4, 2, 32 );
    material = new THREE.MeshBasicMaterial( {color: 0xffffff} );
    material2 = new THREE.MeshBasicMaterial( {color: 0x000000} );
    
    for(var i=0;i<8;i++)
    {
	if(i<4)
	{
	    piece[i] = new THREE.Mesh( geometry, material );
	    piece[i].type = "black";
	}
	else
	{
	    piece[i] = new THREE.Mesh( geometry, material2 );
	    piece[i].type = "white";
	}
	
	scene.add( piece[i] );
	piece[i].speedx = 0;
	piece[i].speedy = 0;
	piece[i].rotateX(-90*Math.PI/180.0);
	piece[i].mass = 2;
    }

    piece[0].position.x = 15;
    piece[0].position.y = 0;

    piece[1].position.x = -15;
    piece[1].position.y = 0;

    piece[2].position.x = 0;
    piece[2].position.y = 15;

    piece[3].position.x = 0;
    piece[3].position.y = -15;

    piece[4].position.x = 10;
    piece[4].position.y = 10;

    piece[5].position.x = -10;
    piece[5].position.y = -10;

    piece[6].position.x = -10;
    piece[6].position.y = 10;

    piece[7].position.x = 10;
    piece[7].position.y = -10;
	
    var geometry = new THREE.Geometry();
    
    geometry.vertices.push(
	new THREE.Vector3( striker.position.x, striker.position.y, striker.position.z),
	new THREE.Vector3( striker.position.x + 20 * Math.sin(striker.theta), striker.position.y + 20*Math.cos(striker.theta), striker.position.z)
    );

    dummy = new THREE.Object3D();
    dummy.position.x = 0;
    dummy.position.y = -70;
    dummy.position.z = 0;
    scene.add( dummy );
    
    geometry = new THREE.CylinderGeometry( 1, 1, 30, 32 );
    material = new THREE.MeshBasicMaterial( {color: 0xff0000} );
    path = new THREE.Mesh( geometry, material );
    dummy.add(path);
    path.position.z = 10;
    path.position.y = 10;

    then = new Date().getTime() / 1000;
    document.body.appendChild( renderer.domElement );
    
    scorevar = document.getElementById("score");

}

function CheckEdgeCollision(one)
{
    if( one.position.x - one.geometry.parameters.radiusTop < edgel.position.x + edgel.geometry.parameters.width/2)
	one.speedx *= -1;

    if(one.geometry.parameters.radiusTop + one.position.x > edger.position.x - edger.geometry.parameters.width/2)
	one.speedx *= -1;

    if(one.geometry.parameters.radiusTop + one.position.y > edgeu.position.y - edgeu.geometry.parameters.height/2)
	one.speedy *= -1;

    if( one.position.y - one.geometry.parameters.radiusTop < edged.position.y + edged.geometry.parameters.height/2)
	one.speedy *= -1;
    
}

document.onkeydown = function(evt)
{
    powerbar = document.getElementById("powermeter");

    if(evt.key == "w")
  	powerbar.value += 1;
  
    if(evt.key == "s")
  	powerbar.value -= 1;
  
    if(evt.key == "a" && striker.state == "inactive" && striker.position.x > -60)
  	striker.position.x -= 1;
  
     if(evt.key == "d" && striker.state == "inactive" && striker.position.x < 60)
	striker.position.x += 1;

    if(evt.key == "f")
    {
	camera.position.x = 0;
	camera.position.y = -200;
	camera.position.z = 200;	
	var a = new THREE.Vector3( 0, 0, 0 );
	camera.lookAt(a);
    }

    if(evt.key == "t")
    {
	camera.position.x = 0;
	camera.position.y = -1;
	camera.position.z = 400;	
	var a = new THREE.Vector3( 0, 0, 0 );
	camera.lookAt(a);
    }

    if(evt.key == "g")
    {
	camera.up.set( 0, 0, 1 );
	toggle = toggle % 9;
	camera.position.x = piece[toggle].position.x + 2;
	camera.position.y = piece[toggle].position.y + 2;
	camera.position.z = piece[toggle].position.z + 25;	
	var a = new THREE.Vector3( striker.position.x,striker.position.y,striker.position.z);
	camera.lookAt(a);
	toggle += 1;
    }
    
    if(evt.key == "r")
    {
	striker.position.x = 0;
	striker.position.y = -70;
	striker.position.z = 12;
	striker.speedx = striker.speedy = 0;
	striker.state = "inactive";

	if(piece[8].flag > 0)
	    piece[8].flag -= 1;
	
	if(piece[8].flag == 0)
	{
	    piece[8].position.x = piece[8].position.y = piece[8].speedx = piece[8].speedy = 0;
	    piece[8].flag = -1;
	}
    }

    if(evt.key == "m")
    {
  	striker.theta += 1;
	dummy.rotateZ(-1*Math.PI/180.0);
    }
    
    if(evt.key == "n")
    {
	striker.theta -= 1;
	dummy.rotateZ(1*Math.PI/180.0);
    }

    if(evt.key == "k" && striker.state == "inactive")
    {
	striker.speedx = powerbar.value * Math.sin(Math.PI*striker.theta/180.0);
	striker.speedy = powerbar.value * Math.cos(Math.PI*striker.theta/180.0);
	striker.state = "active";
    }
};

function CheckPocket(one)
{
    for(var i=0 ;i<4;i++)
    {
	if((one.position.x < corners[i].position.x + 5 && one.position.x > corners[i].position.x - 5) && (one.position.y < corners[i].position.y + 5 && one.position.y > corners[i].position.y - 5))
	{
	    one.speedx=one.speedy=0;
	    one.position.x = -300;
	    if(one.type == "white")
	    {
		netscore += 5;
		if(piece[8].flag == 1)
		{
		    netscore += 50;
		    piece[8].flag == -2;
		}
		    
	    }
	    else if(one.type == "black")
		netscore -= 20;
	    else if(one.type == "queen")
		one.flag = 2
	}
    }
}

function CheckPieceCollision(one,two)
{
    if(one.geometry.parameters.radiusTop + two.geometry.parameters.radiusTop > one.position.distanceTo(two.position))
    {
	console.log("Collision Detected");
	var tempx1 = eloss * (one.speedx * (one.mass - two.mass) + (2 * two.mass * two.speedx)) / (one.mass + two.mass);
	var tempy1 = eloss * (one.speedy * (one.mass - two.mass) + (2 * two.mass * two.speedy)) / (one.mass + two.mass);
	var tempx2 = eloss * (two.speedx * (two.mass - one.mass) + (2 * one.mass * one.speedx)) / (one.mass + two.mass);
	var tempy2 = eloss * (two.speedy * (two.mass - one.mass) + (2 * one.mass * one.speedy)) / (one.mass + two.mass);

	console.log(one.speedy);
	console.log(tempy1);
	
	one.speedx = tempx1;
	one.speedy = tempy1;
	two.speedx = tempx2;
	two.speedy = tempy2;

	one.position.x += one.speedx;
	one.position.y += one.speedy;
	two.position.x += two.speedx;
	two.position.y += two.speedy;
    }
}

function CheckPieceCollision2(one,two)
{
    if(one.geometry.parameters.radiusTop + two.geometry.parameters.radiusTop > one.position.distanceTo(two.position))
    {
	var a = new THREE.Vector2( one.position.x,one.position.y);
	var b = new THREE.Vector2( two.position.x,two.position.y);

	var onetotwo = b.sub(a);

	c = new THREE.Vector2(one.position.x + one.speedx,one.position.y + one.speedy);

	var onevec = c.sub(a);
	var resultant =  onevec.dot(onetotwo)/onetotwo.length() ;

	var theta = onetotwo.angle(); //radians angle with x axis
	
	two.speedx += resultant * Math.cos(theta);
	two.speedy += resultant * Math.sin(theta);
	one.speedx -= resultant * Math.cos(theta);
	one.speedy -= resultant * Math.sin(theta);

	one.position.x += one.speedx;
	one.position.y += one.speedy;
	two.position.x += two.speedx;
	two.position.y += two.speedy;
	
    }
}

function animate()
{
    now = new Date().getTime() / 1000;
    
    requestAnimationFrame( animate );

    if(now - then > 5)
    {
	if(netscore > 0)
	{
	    netscore -= 1;
	    then = now;
	}
    }
    
    CheckEdgeCollision(striker);

    for(var i=0;i<=8;i++)
    {
	CheckEdgeCollision(piece[i]);
	CheckPieceCollision2(striker,piece[i]);
	for(var j=i+1;j<=8;j++)
	{
	    CheckPieceCollision2(piece[i],piece[j]);
	    CheckPieceCollision2(piece[j],piece[i]);	    
	} 
   }
    
    striker.position.x += striker.speedx;
    striker.position.y += striker.speedy

    if(striker.state == "inactive")
    {
	dummy.position.x = striker.position.x;
	dummy.position.y = striker.position.y;
    }
    
    if(Math.abs(striker.speedx) < 0.04)
	striker.speedx = 0;
    else
	striker.speedx += (Math.abs(striker.speedx)/striker.speedx) * friction;

    if(Math.abs(striker.speedy) < 0.04)
	striker.speedy = 0;
    else
	striker.speedy += (Math.abs(striker.speedy)/striker.speedy) * friction;

    if(camera.position.x != 0)
    {
	camera.position.x = piece[toggle].position.x + 2;
	camera.position.y = piece[toggle].position.y + 2;
	camera.position.z = piece[toggle].position.z + 25;
    }
    
    for(var i=0;i<=8;i++)
    {
	piece[i].position.x += piece[i].speedx;
	piece[i].position.y += piece[i].speedy;
	piece[i].position.z = 12;

	if(Math.abs(piece[i].speedx) < 0.04)
	    piece[i].speedx = 0;
	else
	    piece[i].speedx += (Math.abs(piece[i].speedx)/piece[i].speedx) * friction;

	if(Math.abs(piece[i].speedy) < 0.04 )
	    piece[i].speedy = 0;
	else
	    piece[i].speedy += (Math.abs(piece[i].speedy)/piece[i].speedy) * friction;

	CheckPocket(piece[i]);
    }

    scorevar.innerHTML = "SCORE: " + netscore;
    renderer.render( scene, camera );
}

